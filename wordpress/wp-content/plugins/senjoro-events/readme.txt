change user info 
mutation {
  updateUser(input:{clientMutationId:"updateUser", id:"dXNlcjo1", firstName: "The username"}) {
    user {
      id
      name
      firstName
    }
  }
}

update user role

mutation {
  updateUser(input:{clientMutationId:"updateUser", id:"dXNlcjo1", firstName: "The username", roles: "senior"}) {
    user {
      id
      name
      firstName
      roles {
        edges {
          node {
            name
          }
        }
      }
    }
  }
}

