<?php
/**
 * Plugin Name:     Senjoro Events
 * Plugin URI:      #
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          Nikas Klimčiauskas
 * Author URI:      dev-root.com
 * Text Domain:     Senjoro-events
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Senjoro_Events
 */

/**
 * Autoloading our files into plugin.
 *
 */
require __DIR__ . '/vendor/autoload.php';

//Have to load this since the name of the file is different then the class name....
require __DIR__ . '/../wp-graphql/wp-graphql.php';

use WPGraphQL\Mutation\PostObjectCreate;
use WPGraphQL\Mutation\PostObjectUpdate;

function register_events(){

	$domain = 'senjorogo';

	$labels = [
		"name" => __("Events", $domain),
		"singular_name" => __("Event", $domain),
	];

	$args = [
		"label" => __("Events", $domain),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"show_in_graphql" => true,
		"graphql_single_name" => "event",
		"graphql_plural_name" => "events",
		"rest_base" => "events",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "events", "with_front" => true],
		"query_var" => true,
		"menu_icon" => "dashicons-text-page",
		"supports" => ['excerpt', "custom-fields", 'title']
	];

	register_post_type("events", $args);
}
add_action('init', 'register_events');

$fields = [
	'user' => [
		'name'	=> '_events_user',
		'_events_user' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'senior' => [
		'name'	=> '_events_senior',
		'_events_senior' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'date' => [
		'name'	=> '_events_date',
		'_events_date' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'duration' => [
		'name'	=> '_events_duration',
		'_events_duration' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'senior' => [
		'name'	=> '_events_senior',
		'_events_senior' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'random_location' => [
		'name'	=> '_events_random_location',
		'_events_random_location' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'description' => [
		'name'	=> '_events_description',
		'_events_description' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'actual_duration' => [
		'name'	=> '_events_actual_duration',
		'_events_actual_duration' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'started_at' => [
		'name'	=> '_events_started_at',
		'_events_started_at' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'user_signature' => [
		'name'	=> '_events_user_signature',
		'_events_user_signature' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'senior_signature' => [
		'name'	=> '_events_senior_signature',
		'_events_senior_signature' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'service_type' => [
		'name'	=> '_events_service_type',
		'_events_service_type' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	],
	'user_note' => [
		'name'	=> '_events_user_note',
		'_events_user_note' => [
			'type' => 'string',
			'description' => 'Some description'
		],
	]
];

$config = [
	'mutationPrefix' 	=> 	'Event',
	'methods'		=>	[
		'create',
		'delete',
		'update'
	],
];

function events_add_custom_box()
{
	add_meta_box(
		'user',           // Unique ID
		'Event meta',  // Box title
		'events_custom_box_html',  // Content callback, must be of type callable
		'events'                  // Post type
    );
}
add_action('add_meta_boxes', 'events_add_custom_box');

function events_custom_box_html(WP_Post $post): void
{
	global $fields;
	$users = get_users( [ 'role__in' => ['administrator', 'user']] );
	$seniors = get_users('role=senior');
	$options = [1, 2, 3, 4];

	setFieldValues($post->ID);

	?>
	<table class="form-table">
		<tbody>
			<tr class="user-role-wrap">
				<th>User*</br><small>Assign event to selected user</small></th>
				<td>
					<select name="<?php echo $fields['user']['name']; ?>" id="<?php echo $fields['user']['name']; ?>" required>
						<?php
							foreach($users as $user){
								echo '<option value="' . $user->user_login . '" ' . selected($fields['user']['value'] === $user->user_login) . '>'
								 . sprintf('%s (%s)', $user->user_login, $user->display_name)
								 . '</option>';
							}
						?>
					</select>
				</td>
				<th>Senior*</br><small>Select event host</small></th>
				<td>
					<select name="<?php echo $fields['senior']['name']; ?>" id="<?php echo $fields['senior']['name']; ?>">
						<?php
							foreach($seniors as $user){
								echo '<option value="' . $user->user_login . '" ' . selected($fields['senior']['value'] === $user->user_login) . '>'
								. sprintf('%s (%s)', $user->user_login, $user->display_name)
								. '</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr class="events_date_duration_container">
				<th>Date & time *</br><small>Enter event start date & time</small></th>
				<td>
					<input type="datetime-local" min="<?php echo sprintf("%sT%s", date("Y-m-d"), date("H:i")) ?>" value="<?php echo $fields['date']['value'] ?>" name="<?php echo $fields['date']['name']; ?>" id="<?php echo $fields['date']['name']; ?>" class="datepicker" required>
				</td>
				<th>Estimated duration *</br><small>Specify approximate event duration</small></th>
				<td>
					<?php
						foreach($options as $option){
							print( '<input type="radio"
										 name="' . $fields['duration']['name'] . '"
										 value="' . $option . '" '
										 . checked($option, $fields['duration']['value'], false) . '
								  > ' . sprintf('%d h.', $option)) . '</input>';
						}
					?>
				</td>
			</tr>
			<tr>
				<th>Allow to start event at any location</th>
				<td>
					<input type="hidden" name="<?php echo $fields['random_location']['name'] ?>" value="no">
					<label class="switch">
						<input type="checkbox" name="<?php echo $fields['random_location']['name'] . '"' . checked( 'yes', $fields['random_location']['value'], false)?>'" value="yes">
						<span class="slider"></span>
					</label>
				</td>
			</tr>
			<tr>
				<th>Description<br><small>Leave a note to event owner</small></th>
				<td colspan="3">
				<textarea id="<?php echo $fields['description']['name'] ?>" name="<?php echo $fields['description']['name'] ?>" rows="4"><?php echo $fields['description']['value']; ?></textarea>
				</td>
			</tr>
			<tr>
				<th colspan="4">Completed Event information<br><small>These fields will be automatically filled on event completion</small></th>
			</tr>
			<tr>
				<td colspan="2">
					<label for="<?php echo $fields['actual_duration']['name'] ?>"><b>Actual Event duration</b><br>
					<input type="text" value="<?php echo $fields['actual_duration']['value'] ?>" name="<?php echo $fields['actual_duration']['name'] ?>" disabled/>
					</label>
				</td>
				<td colspan="2">
					<label for="<?php echo $fields['started_at']['name'] ?>"><b>Event was started at</b><br>
					<input type="text" value="<?php echo $fields['started_at']['value'] ?>" name="<?php echo $fields['started_at']['name'] ?>" disabled/>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="<?php echo $fields['user_signature']['name'] ?>"><b>User signature</b><br>
					<input type="text" value="<?php echo $fields['user_signature']['value'] ?>" name="<?php echo $fields['user_signature']['name'] ?>" disabled/>
					</label>
				</td>
				<td colspan="2">
					<label for="<?php echo $fields['senior_signature']['name'] ?>"><b>Senior signature</b><br>
					<input type="text" value="<?php echo $fields['senior_signature']['value'] ?>" name="<?php echo $fields['senior_signature']['name'] ?>" disabled/>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<label for="<?php echo $fields['service_type']['name'] ?>"><b>Service type</b><br>
					<input type="text" value="<?php echo $fields['service_type']['value'] ?>" name="<?php echo $fields['service_type']['name'] ?>" disabled/>
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<label for="<?php echo $fields['user_note']['name'] ?>"><b>User note</b><br>
					<textarea type="text" name="<?php echo $fields['user_note']['name'] ?>" disabled><?php echo $fields['user_note']['value'] ?></textarea>
					</label>
				</td>
			</tr>
		</tbody>
	</table>
	<?php
}

function setFieldValues(int $postId): array{

	global $fields;

	foreach($fields as $key => $field) {
		$fields[$key]['value'] = get_post_meta($postId, $field['name'])['0'] ?? null;
	}

	return $fields;
}

function events_save_postdata($post_id)
{
	global $fields;

	foreach($fields as $field){
		if (array_key_exists($field['name'], $_POST)) {
			update_post_meta(
				$post_id,
				$field['name'],
				$_POST[$field['name']]
			);
		}
	}
}
add_action('save_post', 'events_save_postdata');

function wpse_cpt_enqueue( $hook_suffix ){
	$cpt = 'events';

	if( in_array($hook_suffix, array('post.php', 'post-new.php') ) ){
		$screen = get_current_screen();

		if( is_object( $screen ) && $cpt == $screen->post_type ){

			wp_register_style( 'events-plugins-style', plugin_dir_url(__FILE__) . '/style.css');
			wp_enqueue_style('events-plugins-style');

		}
	}
}

add_action( 'admin_enqueue_scripts', 'wpse_cpt_enqueue');

/*
 * Register wordpress meta fields from the list to graphql.
 * This allows to query custom meta fields, nothing else.
 * @ToDo Add some kind of authentication if required.
 */
add_action( 'graphql_register_types', function() {

	global $fields, $args;

	foreach($fields as $field){
		register_graphql_field( 'event' , $field['name'], [
			'type' => 'String',
			'description' => __( 'Custom_field', 'wp-graphql' ),
			'resolve' => function( $post ) use($field) {
			 global $fields;
			 return get_post_meta( $post->ID, $field['name'], true );
			}
		 ] );
	}
  } );

add_action( 'graphql_post_object_mutation_update_additional_data', 'graphql_register_edit_mutation', 10, 5 );

function theTest(){
	//Pass $fields from global scope so we can access it.
	global $fields;

	/*
	 * @var Array
	 * Will hold addition my generated fields here;
	 */
	$addition_event_fields = [];

	//Join two array into one
	if(isset($fields)) {
		foreach ($fields as $key => $value) {
			$addition_event_fields[$value['name']] = $value[$value['name']];
		}
	}

	$event_obj = get_post_type_object('events');

	$create_fields = array_merge(PostObjectCreate::get_input_fields($event_obj), $addition_event_fields);
	$update_fields = array_merge(PostObjectUpdate::get_input_fields($event_obj), $addition_event_fields);
	$delete_fields = array_merge(PostObjectCreate::get_input_fields($event_obj), $addition_event_fields);


	register_graphql_mutation(
		'createEvent',
		[
			'inputFields' 		  => $create_fields,
			'outputFields'        => PostObjectCreate::get_output_fields( $event_obj ),
			'mutateAndGetPayload' => PostObjectCreate::mutate_and_get_payload( $event_obj, 'createEvent' ),
		]
	);	register_graphql_mutation(
		'updateEvent',
		[
			'inputFields' => $update_fields,
			'outputFields'        => PostObjectUpdate::get_output_fields( $event_obj ),
			'mutateAndGetPayload' => PostObjectUpdate::mutate_and_get_payload( $event_obj, 'updateEvent' ),
		]
	);	register_graphql_mutation(
		'deleteEvent',
		[
			'inputFields'	=> $delete_fields,
			'outputFields'        => \WPGraphQL\Mutation\PostObjectDelete::get_output_fields( $event_obj ),
			'mutateAndGetPayload' => \WPGraphQL\Mutation\PostObjectDelete::mutate_and_get_payload( $event_obj, 'deleteEvent' ),
		]
	);
}
add_action('wp_loaded', 'theTest');

