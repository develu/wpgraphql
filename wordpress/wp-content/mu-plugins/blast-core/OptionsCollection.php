<?php
/**
 * by vitalikaz (14/04/15 11:31)
 *
 * Class helps to manipulate with options-framework options and register all the options added
 */

namespace Blast;

class OptionsCollection
{
    const TYPE_TEXT = 'text',
        TYPE_TEXTAREA = 'textarea',
        TYPE_HEADING = 'heading',
        TYPE_EDITOR = 'editor',
		TYPE_CHECKBOX = 'checkbox',
        TYPE_IMAGE = 'upload';

    private $title = '';
    private $options = [];

    public function __construct($title)
    {
        $this->title = $title;
    }

    /**
     * Add new option to collection
     *
     * @param $title
     * @param $id
     * @param $type
     * @param array $options
     */
    public function option($title, $id, $type, array $options = null)
    {
        $option = [
            'name' => $title,
            'id' => $id,
            'type' => $type
        ];

        if ($options) {
            $option['options'] = $options;
        }

        $option = $this->formatOption($option);

        $this->options[] = $option; //$option;
    }

    /**
     * Format option for custom cases (such as posts list, etc.)
     *
     * @param $option
     * @return mixed
     */
    private function formatOption($option)
    {
        switch($option['type']) {

        }

        return $option;
    }

    /**
     * Get formatted options
     *
     * @return array
     */
    public function getOptions()
    {
        /**
         * Prepend options with heading and return
         */
        return array_merge([['name' => $this->title, 'type' => self::TYPE_HEADING]], $this->options);
    }

}