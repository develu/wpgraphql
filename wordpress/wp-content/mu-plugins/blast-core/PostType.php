<?php
/**
 * by vitalikaz (14/04/15 15:34)
 *
 * Class helps working with custom post type registrations
 */

namespace Blast;

class PostType
{

    protected $name;
    /**
     * Current type data
     * @var array
     */
    protected $data = [];

    public function __construct($name, $label = null)
    {
        $this->name = $name;
        $this->data['label'] = $label ? $label : ucwords(str_replace(['_', '-'], [' ', ' '], $name));
    }

    /**
     * Generates and sets type labels array
     *
     * @param array $prefixes Must have keys: all, add, edit, new, new_accusative
     * @param array $names Must have keys: single, plural, accusative
     * @return $this
     */
    public function generateLabels(array $prefixes, array $names)
    {
        $this->data['labels'] = [
            'singular_name' => $names['single'],
            'all_items' => sprintf('%s %s', ucfirst($prefixes['all']), $names['plural']),
            'add_new' => sprintf('%s %s', ucfirst($prefixes['add']), $names['accusative']),//__('Pridėti Darbo pasiūlymų kategoriją', 'vac'),
            'add_new_item' => sprintf('%s %s %s', ucfirst($prefixes['add']), $prefixes['new_accusative'], $names['accusative']),//__('Pridėti naują Darbo pasiūlymų kategoriją', 'vac'),
            'edit_item' => sprintf('%s %s', ucfirst($prefixes['edit']), $names['accusative']),//__('Redaguoti Darbo pasiūlymų kategoriją', 'vac'),
            'new_item' => sprintf('%s %s', ucfirst($prefixes['new']), $names['single']) //__('Nauja Darbo pasiūlymų kategorija', 'vac')
        ];

        $this->data['label'] = ucwords($names['plural']);

        return $this;
    }

    /**
     * Makes type publicly visible & querable
     *
     * @param $public
     * @param bool $hasArchive
     * @param null $slug
     * @return $this
     */
    public function setPublic($public, $hasArchive = true, $slug = null)
    {
        $this->data['has_archive'] = $hasArchive;

        if ($public) {
            $this->data['public'] = true;
            $this->data['show_ui'] = true;
            if ($slug !== null) {
                $this->data['rewrite']['slug'] = $slug;
            }
        } else {
            $this->data['public'] = false;
        }

        return $this;
    }

    /**
     * Possibility to have URL structure like /post-type/taxonomy-first-term-slug/post-slug
     *
     * @param string $taxonomySlug
     * @return $this
     */
    public function setTaxonomyBase($taxonomySlug)
    {
        if (empty($this->data['rewrite']['slug'])) {
            return $this;
        }

        add_action('init', function() {
            $slug = explode('/', $this->data['rewrite']['slug'])[0];
            // leave archive link here
            add_rewrite_rule(
                sprintf('%s/?$', $slug),
                sprintf('index.php?post_type=%s', $this->getName()),
                'top'
            );

            add_rewrite_rule(
                sprintf('%s/page/([0-9]+)?$', $slug),
                sprintf('index.php?post_type=%s&paged=$matches[1]', $this->getName()),
                'top'
            );
        });

        add_filter('post_type_link', function($postLink, $id = 0) use ($taxonomySlug) {
            $post = get_post($id);
            if (!is_object($post) || $post->post_type != $this->name) {
                return $postLink;
            }

            if ($terms = wp_get_object_terms($post->ID, $taxonomySlug)) {
                $term = $terms[0]->slug;

                return str_replace('%' . $taxonomySlug . '%', $term, $postLink);
            }

            return $postLink;
        }, 1, 3);

        add_filter('post_type_archive_link', function($link) use($taxonomySlug) {
            return str_replace('/%' . $taxonomySlug . '%', '', $link);
        });

        return $this;
    }

    /**
     * Adds supports things to type
     *
     * @param array $supports
     * @return $this
     */
    public function supports(array $supports)
    {
        $this->data['supports'] = $supports;

        return $this;
    }

    /**
     * Makes taxonomy hierarchical or not
     *
     * @param boolean $hierarchical
     * @return $this
     */
    public function hierarchical($hierarchical = true)
    {
        $this->data['hierarchical'] = $hierarchical;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
}
