<?php
/**
 * by vitalikaz (14/04/15 16:33)
 */

namespace Blast;


class Taxonomy extends PostType {

    private $useWith = [];

    /**
     * Adds useWith capability for taxonomy
     *
     * @param array $useWith
     * @return $this
     */
    public function useWith(array $useWith) {
        $this->useWith = $useWith;

        return $this;
    }

    public function getUseWith() {
        return $this->useWith;
    }
}