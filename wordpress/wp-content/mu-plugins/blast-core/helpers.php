<?php
/**
 * by vitalikaz (13/04/15 19:41)
 */

/**
 * Some shorters
 */
function template_uri($path = null)
{
    return get_template_directory_uri() . ($path !== null ? '/' . $path : '');
}

function template_path($path = null)
{
    return get_template_directory() . ($path !== null ? '/' . $path : '');
}

/**
 * Delegate adding CSS
 *
 * @param $css
 */
function css($css)
{
    Blast\ThemeCore::instance()->css($css);
}

/**
 * Delegate adding JS
 *
 * @param $js
 */
function js($js)
{
    Blast\ThemeCore::instance()->js($js);
}

/**
 * Helper that mixes WordPress native thumbnails & ACF's image fields facilities
 *
 * @param $size
 * @param null $defaultImage
 * @param null $field_key
 * @param null $post_id
 * @return null|string Returns ACF image if there's one by contidions (ACF plugin activated & existing field_key passed) OR post's featured image OR a placeholder otherwise
 */
function the_post_image($size, $defaultImage = null, $field_key = null, $post_id = null)
{
    $imageHtml = null;
    $post_id = $post_id ? $post_id : get_the_ID();

    /**
     * If no field_key passed or no ACF plugin - try to get post's featured image
     */
    if (!$field_key || !function_exists('get_field')) {
        $imageHtml = get_the_post_thumbnail($post_id, $size);
    }
    /**
     * And override it by ACF's image with field_key if passed
     */
    if ($field_key && function_exists('get_field')) {

        $attachment_id = get_field($field_key, $post_id);
        if (intval($attachment_id) > 0) {
            // return an image HTML
            $imageHtml = wp_get_attachment_image($attachment_id, $size);
        }
    }

    /**
     * Or use placeholder for image of non is passed (use default thumb or placehold.it service for that)
     */
    if (!$imageHtml && $defaultImage) {
        $imageHtml = sprintf(
            '<img src="%s" />',
            $defaultImage
        );

    }

    print $imageHtml;
}

/**
 * @param $size
 * @param null $defaultImage
 * @param null $field_key
 * @param null $post_id
 * @return null
 */
function get_post_image_url($size, $defaultImage = null, $field_key = null, $post_id = null)
{
    ob_start();
    the_post_image($size, $defaultImage, $field_key, $post_id);
    $imageHtml = ob_get_clean();
    $doc = new DOMDocument();
    $doc->loadHTML($imageHtml);
    $imageTags = $doc->getElementsByTagName('img');
    if ($imageTags) {
        return $imageTags->item(0)->getAttribute('src');
    }

    return null;
}

/**
 * Fetches $posts to $template
 *
 * @param string $template Template to fetch data to (should use the_* methods, as POST will be set for them)
 * @param array $data Data to fetch
 * @param null $noPostsTemplate Template name to show if $posts is empty
 */
function loop_posts($template, array $data, $noPostsTemplate = 'templates/part-nothing-found.php')
{
    if (count($data)) {
        global $post;

        foreach ($data as $loopPostIndex => $post) {
            setup_postdata($post);
            include locate_template($template);
        }
        wp_reset_postdata();
    } else if ($noPostsTemplate !== null) {
        include locate_template($noPostsTemplate);
    }
}

/**
 * Outputs the menu with custom walker
 *
 * @param $name
 */
function menu($name)
{
    wp_nav_menu(array(
            'walker' => new Blast\MenuWalker(),
            'theme_location' => $name,
            'container' => false,
            'items_wrap' => '%3$s',
            'menu_class' => ''
        )
    );
}

/**
 * Just returns a current taxonomy
 *
 * @return mixed
 */
function current_taxonomy()
{
    global $wp_query;
    return $wp_query->queried_object;
}

/**
 * Outputs the breadcrumbs
 */
function the_breadcrumbs()
{
    global $post;
    $siteName = 'LavinuVaika.LT';
    $parts = [
        [
            'link' => home_url(),
            'title' => $siteName
        ]
    ];

    if (is_post_type_archive() || is_single() || is_tax()) {
        if (is_post_type_archive()) {
            $postType = get_queried_object();
        }
        if (is_single()) {
            $postType = get_post_type_object($post->post_type);
        }
        if (is_tax()) {
            global $wp_taxonomies;
            $postType = get_post_type_object($wp_taxonomies[get_queried_object()->taxonomy]->object_type[0]);
        }
        $parts[] = [
            'link' => get_post_type_archive_link($postType->name),
            'title' => $postType->labels->name
        ];

        if (is_single()) {
            $taxonomies = get_post_taxonomies($post);
            if (count($taxonomies)) {
                $terms = wp_get_post_terms($post->ID, $taxonomies[0]);
                if (count($terms) && $terms[0]->taxonomy != 'language') {
                    $parts[] = [
                        'link' => get_term_link($terms[0]),
                        'title' => $terms[0]->name
                    ];
                }
            }

            $parts[] = [
                'link' => get_the_permalink($post),
                'title' => $post->post_title
            ];
        }
        if (is_tax()) {
            $parts[] = [
                'link' => get_term_link(get_queried_object()),
                'title' => get_queried_object()->name
            ];
        }
    }

    if (is_page()) {
        $ancestors = get_post_ancestors($post->ID);
        foreach ($ancestors as $ancestor) {
            $ancestorObject = get_post($ancestor);
            $parts[] = [
                'link' => get_the_permalink($ancestorObject),
                'title' => $ancestorObject->post_title
            ];
        }
        $parts[] = [
            'link' => get_the_permalink($post),
            'title' => $post->post_title
        ];
    }


    $partsCount = count($parts);
    if ($partsCount) {
    ?>
        <nav id="breadcrumb">
           <ul>
            <?php foreach ($parts as $i => $part): ?>
                <?php if ($i + 1 == $partsCount): ?>
                    <li class="current"><?php print esc_html($part['title']); ?></li>
                <?php else: ?>
			   		<li><a href="<?php print $part['link']; ?>"><?php print esc_html($part['title']); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
			</ul>
        </nav>
    <?php }

}

/**
 * @param WP_Post[] $posts
 * @param string $taxonomy Taxonomy to group by
 * @return array
 */
function group_posts_by_category(array $posts, $taxonomy)
{
    $groupedPosts = [0 => ['posts' => []]];

    foreach ($posts as $post) {
        $terms = wp_get_post_terms($post->ID, $taxonomy);
        if ($terms) {
            foreach ($terms as $term) {
                if (!array_key_exists($term->term_id, $groupedPosts)) {
                    $groupedPosts[$term->term_id] = ['term' => $term, 'posts' => []];
                }

                $groupedPosts[$term->term_id]['posts'][] = $post;
            }
        } else {
            $groupedPosts[0]['posts'][] = $post;
        }
    }

    return $groupedPosts;
}

/**
 * @param null $class
 * @return null|string
 */
function bodyClass($class = null)
{
    return \Blast\ThemeCore::instance()->bodyClass($class);
}

function the_body_class()
{
    $classes = [];
    global $post;
    global $currentMenuItem;
    $fields = get_fields($post);

    if (is_home()) {
        $classes[] = 'home';
    }

    if (is_page()) {
        $classes[] = 'page';
    }

    if (is_404()) {
        $classes[] = 'page404';
    }

    if ($fields && array_key_exists('extra_body_class', $fields)) {
        $classes = array_merge($classes, explode(' ', $fields['extra_body_class']));
    }

    if (is_post_type_archive() || is_tax()) {
        $classes[] = 'loop';
        if (is_post_type_archive()) {
            $classes[] = $post->post_type;
        } else {
            global $wp_taxonomies;
            $classes[] = $wp_taxonomies[current_taxonomy()->taxonomy]->object_type[0];

        }
    }

    if (is_single()) {
        $classes[] = 'single';
        if ($post) {
            $classes[] = $post->post_type;
            $taxonomies = get_object_taxonomies( $post->post_type, 'objects' );
            if ($taxonomies) {
                foreach ($taxonomies as $postTax) {
                    $terms = get_the_terms($post, $postTax->name);
                    if ($terms) {
                        foreach ($terms as $term) {
                            $class = get_field('extra_body_class', $term);
                            if ($class) {
                                $classes[] = $class;
                            }
                        }
                    }
                }
            }
        }
    }
    if ($currentMenuItem) {
        $description = trim($currentMenuItem->post_content);
        if ($description) {
            $classes[] = $description;
        }
    }

    if (!count($classes)) {
        return '';
    } else {
        return sprintf(' class="%s"', implode(' ', $classes));
    }
}

function get_featured_ad()
{
    $image = of_get_option('featured_image');
    if ($image) {
        return [
            'image' => $image,
            'link' => of_get_option('featured_link')
        ];
    }

    return null;
}
