<?php
/**
 * The Base class of theme-specific things
 *
 * by vitalikaz (13/04/15 18:52)
 */
namespace Blast;

class ThemeCore
{

    /**
     * Singleton
     */
    private static $instance = null;

    /**
     * Bunch of CSS files for registering in theme
     *
     * @var array
     */
    private $css = [];

    /**
     * Bunch of JS files for registering in theme
     *
     * @var array
     */
    private $js = [];

    /**
     * Array of closures to call on after_theme_setup
     * Contains such things as menus registrations, thumbnail sizes, etc.
     *
     * @var array of Closures
     */
    private $themeSetup = [];

    /**
     * Saves things related to options-framework options to batch-register them
     *
     * @var array of OptionsCollection
     */
    private $optionsCollections = [];

    /**
     * Saves post types to register them in a batch on init
     *
     * @var array of PostType
     */
    private $postTypes = [];

    /**
     * ... and same for taxonomies
     * @var array of Taxonomy
     */
    private $taxonomies = [];

    /**
     * @var array of sidebars to be registered
     */
    private $sidebars = [];

    /**
     * @var array of widgets to be registered
     */
    private $widgets = [];

    /**
     * @var array of shortcodes. ['shortcode' => callback]
     */
    private $shortcodes = [];

    /**
     * @var array of stylesheets to attach to WP editor
     */
    private $editorStyles = [];

    /**
     * @var array CSS body classes
     */
    private $bodyClasses = [];

    /**
     * Singleton getter
     *
     * @return ThemeCore
     */
    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Inits theme
     */
    public function init()
    {
        /**
         * Init styles & scripts
         */
        add_action('wp_enqueue_scripts', function () {
            $this->initCss();
            $this->initJs();
        });

        /**
         * Init after_theme_setup
         */
        add_action('after_setup_theme', function () {
            $this->setupTheme();
        });

        /**
         * Register options with it's framework if needed
         */
        $this->registerOptionsArray();

        /**
         * Widgets-related things
         */
        add_action('widgets_init', function () {
            $this->registerSidebars();
        });

        /**
         * Postponed post types & taxonomies registration
         */
        add_action('init', function () {
            $this->registerPostTypes();
            $this->registerTaxonomies();
            $this->registerShortCodes();
            if (count($this->editorStyles)) {
                add_editor_style($this->editorStyles);
            }
        });

    }

    /**
     * Enqueues styles to WordPress
     */
    private function initCss()
    {
        foreach ($this->css as $css) {
            if (substr($css, -4) == '.css') {
                $css = template_uri($css);
                wp_enqueue_style(basename($css), $css, [], filemtime(template_path()));
            } else {
                wp_enqueue_style($css);
            }
        }
    }

    /**
     * Enqueues scripts to Wordpress
     */
    private function initJs()
    {
        foreach ($this->js as $js) {
            if (substr($js, -3) == '.js') {
                $js = template_uri($js);
                wp_enqueue_script(basename($js), $js, [], filemtime(template_path()));
            } else {
                wp_enqueue_script($js);
            }
        }
    }

    /**
     * Calls theme-setup closures
     */
    private function setupTheme()
    {
        foreach ($this->themeSetup as $closure) {
            $closure();
        }
    }

    /**
     * Adds CSS files to WP queue
     *
     * @param $files
     * @return $this
     */
    public function css($files)
    {
        /**
         * Adds CSS files for queueing to WP
         */
        if (is_array($files)) {
            $this->css = array_merge($this->css, $files);
        } else {
            $this->css[] = $files;
        }

        return $this;
    }

    /**
     * Adds JS files to WP queue
     *
     * @param $files
     * @return $this
     */
    public function js($files)
    {
        /**
         * Adds JS files for queueing to WP
         */
        if (is_array($files)) {
            $this->js = array_merge($this->js, $files);
        } else {
            $this->js[] = $files;
        }

        return $this;
    }

    /**
     * Clears up header with semi-unnecessary things
     *
     * @return $this
     */
    public function clearHeader()
    {
        remove_action('wp_head', 'feed_links');
        remove_action('wp_head', 'feed_links_extra');
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'parent_post_rel_link');
        remove_action('wp_head', 'start_post_rel_link');
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'wp_shortlink_wp_head');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
		
        return $this;
    }

    /**
     * Adds support for styling a WP editor ( editor-style.css = default)
     *
     * @param null $styleCss
     * @return $this
     */
    public function editorStyle($styleCss = 'editor-style.css')
    {
        $this->editorStyles[] = $styleCss;

        return $this;
    }

    /**
     * Registers menus within WordPress
     *
     * @param array $menus
     * @return $this
     */
    public function menus(array $menus)
    {
        $this->themeSetup[] = function () use ($menus) {
            register_nav_menus($menus);
        };

        return $this;
    }

    /**
     * Adds custom thumbnails/images sizes
     *
     * @param array $thumbs 'name' => [ width, height, crop? ]
     * @return $this
     */
    public function thumbs(array $thumbs)
    {
        $this->themeSetup[] = function () use ($thumbs) {
            add_theme_support('post-thumbnails');

            foreach ($thumbs as $i => $ts) {
                add_image_size($i, $ts[0], $ts[1], (isset($ts[2]) ? $ts[2] : true));
            }
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function enableWooCommerce()
    {
        $this->themeSetup[] = function() {
            add_theme_support( 'woocommerce' );
        };

        remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
        remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

        return $this;
    }

    /**
     * Decorate & save post type
     *
     * @param $name string Name of new custom post type
     * @param callable|\Closure $closure Decorator
     * @param string|null $label
     * @return $this
     */
    public function postType($name, \Closure $closure, $label = null)
    {
        $postType = new PostType($name, $label);
        $closure($postType);
        $this->postTypes[] = $postType;

        return $this;
    }

    /**
     * Decorate & save custom taxonomies
     *
     * @param $name
     * @param callable|\Closure $closure
     * @param string|null $label
     * @return $this
     */
    public function taxonomy($name, \Closure $closure, $label = null)
    {
        $taxonomy = new Taxonomy($name, $label);
        $closure($taxonomy);
        $this->taxonomies[] = $taxonomy;

        return $this;
    }

    /**
     * Adds new OptionsCollection by closure's settings.
     * NOTICE: requires options-framework WordPress plugin to be installed & enabled to work
     *
     * @param string $title Settings heading group title
     * @param callable|\Closure $optionsClosure function($options) { $options->option(...); }
     * @return $this
     */
    public function options($title, \Closure $optionsClosure = null)
    {
        $optionsCollection = new OptionsCollection($title);
        if ($optionsClosure) {
            $optionsClosure($optionsCollection);
        }

        $this->optionsCollections[] = $optionsCollection;
        return $this;
    }

    /**
     * Registers a sidebar
     *
     * @param $name
     * @param array $options @see https://codex.wordpress.org/Function_Reference/register_sidebar
     * @return $this
     */
    public function sidebar($name, $options = [])
    {
        $opts = array_merge([
            'id' => $name,
            'before_widget' => '<section class="widget %2$s">',
            'after_widget' => '</section>'
        ], $options);
        $this->sidebars[] = $opts;

        return $this;
    }

    /**
     * Registers a widget
     *
     * @param $className
     * @return $this
     */
    public function widget($className)
    {
        $this->widgets[] = $className;

        return $this;
    }

    /**
     * Registers a shortcode
     *
     * @param $name
     * @param $callback
     * @return $this
     */
    public function shortcode($name, $callback)
    {
        $this->shortcodes[$name] = $callback;

        return $this;
    }

    /**
     * Merges all added OptionsCollections to one array of options
     * and registers these options within WordPress Options Framework
     */
    private function registerOptionsArray()
    {
        $options = [];
        foreach ($this->optionsCollections as $optionsCollection) {
            $options = array_merge($options, $optionsCollection->getOptions());
        }

        /**
         * Add options FW filter to pass options there
         */
        if ($options) {
            add_filter('of_options', function ($_opt) use ($options) {
                return array_merge($_opt ? $_opt : [], $options);
            });

        }
    }

    /**
     * Runs through added post types & registers them
     */
    private function registerPostTypes()
    {
        foreach ($this->postTypes as $postType) {
            register_post_type($postType->getName(), $postType->getData());
        }
    }

    /**
     * Runs through added post types & registers them
     */
    private function registerTaxonomies()
    {
        foreach ($this->taxonomies as $taxonomy) {
            register_taxonomy($taxonomy->getName(), $taxonomy->getUseWith(), $taxonomy->getData());
        }

        add_filter('generate_rewrite_rules', [$this, 'allowSameTaxonomiesPostSlugs']);
    }

    /**
     * Runs throught added sidebars & registers them
     */
    private function registerSidebars()
    {
        // Unregister default widgets
        unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Archives');
        unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Search');
        unregister_widget('WP_Widget_Text');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Widget_Recent_Posts');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Nav_Menu_Widget');
        unregister_widget('Twenty_Eleven_Ephemera_Widget');

        foreach ($this->sidebars as $sidebar) {
            register_sidebar($sidebar);
        }

        foreach ($this->widgets as $widget) {
            register_widget($widget);
        }
    }

    /**
     * Registers current short codes
     */
    public function registerShortCodes()
    {
        foreach ($this->shortcodes as $name => $callback) {
            add_shortcode($name, $callback);
        }

    }

    public function allowSameTaxonomiesPostSlugs()
    {
        global $wp_rewrite;

        $rules = array();

        $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
        $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');

        foreach ($post_types as $post_type) {
            foreach ($taxonomies as $taxonomy) {
                foreach ($taxonomy->object_type as $object_type) {
                    if ($object_type == $post_type->rewrite['slug']) {

                        $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));

                        foreach ($terms as $term) {
                            $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                        }
                    }
                }
            }
        }
        $wp_rewrite->rules = array_merge($rules, $wp_rewrite->rules);
    }

    /**
     * @param string|null $class
     * @return string|null
     */
    public function bodyClass($class = null)
    {
		$classes = [];
		global $post;
		global $currentMenuItem;
		$fields = get_fields($post);

		if (is_home()) {
			$classes[] = 'home';
		}

		if (is_page()) {
			$classes[] = 'page';
		}
		
		if (is_404()) {
			$classes[] = 'page404';
		}

		if ($fields && array_key_exists('extra_body_class', $fields)) {
			$classes = array_merge($classes, explode(' ', $fields['extra_body_class']));
		}

		if (is_post_type_archive() || is_tax()) {
			$classes[] = 'loop';
			if (is_post_type_archive()) {
				//$classes[] = $post->post_type;
			} else {
				global $wp_taxonomies;
				$classes[] = $wp_taxonomies[current_taxonomy()->taxonomy]->object_type[0];

			}
		}

		if (is_single()) {
			$classes[] = 'single';
			if ($post) {
				$classes[] = $post->post_type;
				$taxonomies = get_object_taxonomies( $post->post_type, 'objects' );
				if ($taxonomies) {
					foreach ($taxonomies as $postTax) {
						$terms = get_the_terms($post, $postTax->name);
						if ($terms) {
							foreach ($terms as $term) {
								$class = get_field('extra_body_class', $term);
								if ($class) {
									$classes[] = $class;
								}
							}
						}
					}
				}
			}
		}
		
		if ($currentMenuItem) {
			$description = trim($currentMenuItem->post_content);
			if ($description) {
				$classes[] = $description;
			}
		}
		
		// woocommerce
		
		if (is_shop()) {
			$classes[] = 'shop';
		}
		
		if (is_cart()) {
			$classes[] = 'cart';
		}
		
		if (is_checkout()) {
			$classes[] = 'checkout';
		}
		
		if (is_wc_endpoint_url( 'order-received' )) {
			$classes[] = 'thank-you';
		}

		if (!count($classes)) {
			return '';
		} else {
			return sprintf(' class="%s"', implode(' ', $classes));
		}
    }
}
