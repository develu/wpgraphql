<?php
/**
 * Plugin Name: BlaST WP Core
 * Description: Adds some init core functions for blast WP structure
 * Version: 1.0
 * Author: vitaliaz
 * Author URI: http://www.blast.lt
 */

require dirname(__FILE__) . '/blast-core/helpers.php';
require dirname(__FILE__) . '/blast-core/ThemeCore.php';
require dirname(__FILE__) . '/blast-core/OptionsCollection.php';
require dirname(__FILE__) . '/blast-core/PostType.php';
require dirname(__FILE__) . '/blast-core/Taxonomy.php';
