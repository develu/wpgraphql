<?php

/**
 * Plugin Name: BlaST Menu Walker
 * Description: Adds ability to use clean menu walker for `ul > li + li.active > a + ul > li` links structure. Use Blast\MenuWalker as a walker class.
 * Version: 1.0
 * Author: vitaliaz
 * Author URI: http://www.blast.lt
 */
namespace Blast;

class MenuWalker extends \Walker
{
    var $tree_type = array('post_type', 'taxonomy', 'custom');
    var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');

    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul>\n";
    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $current_object_id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $classes = empty($item->classes) ? [] : (array)$item->classes;
		
        // check if need "active" class
        $_classes = [];
        $postTypeObject = $item ? get_post_type_object(get_post_type($item->object_id)) : null;
		
		if (!empty($classes[0])) {
            $_classes[] = $classes[0];
        }
        
		if (in_array('current-menu-item', $classes) ||
            in_array('current-post-parent', $classes) ||
            in_array('current-menu-ancestor', $classes) ||
            in_array('current-menu-parent', $classes) ||
            is_single() && $postTypeObject && $item->title == $postTypeObject->label
        ) {
            $_classes[] = 'active';
        }

        if (in_array('menu-item-has-children', $classes)) {
            $_classes[] = 'has-child';
        }

        $classes = $_classes;


        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = strlen(trim($class_names)) > 0 ? ' class="' . esc_attr($class_names) . '"' : '';

        $output .= $indent . '<li' . $class_names . '>';
        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

        $item_output = '<a' . $attributes . '>';
        $item_output .= apply_filters('the_title', $item->title, $item->ID);
        $item_output .= '</a>';
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
		
    }

    function end_el(&$output, $item, $depth = 0, $args = array())
    {
        $output .= "</li>\n";
    }
}

?>
