<?php

// ACF INIT
require_once __DIR__ . '/acf.php';

/**
 * Common inits...
 */
Blast\ThemeCore::instance()
    /**
     * Scripts & styles
     */
    ->css([
        'style.css',
    ])
    ->js([
        //'jquery',
    ])
    ->clearHeader()
    /**
     * Images settings
     */
    ->thumbs([
        'avatar-thumb' => [150, 150, true],
        'avatar' => [300, 300, true],
    ])
    /**
     * Menus
     */
    ->menus([
        'main' => __('Main Menu'),
    ])
    /**
     * Theme options
     */
    ->options(__('SenjoroGo Contact information', 'theme'), function (\Blast\OptionsCollection $o) {
        $o->option(__('E-mail'), 'email', \Blast\OptionsCollection::TYPE_TEXT);
        $o->option(__('Address'), 'address', \Blast\OptionsCollection::TYPE_TEXT);
        $o->option(__('Phone'), 'phone', \Blast\OptionsCollection::TYPE_TEXT);
        $o->option(__('Url'), 'url', \Blast\OptionsCollection::TYPE_TEXT);
    })
    ->init();

// JWT TOOKEN

add_filter( 'graphql_jwt_auth_secret_key', function() {
  return 'tHMImiv&-Jez**i=vm#~Ex|KSGYD^s-Qi^;#0Ve&<8cW{36T 8SY<)}Sh-pm33u-';
});

// SET AUTH TOKEN EXPIRATION TO 36 DAYS
add_filter( 'graphql_jwt_auth_expire', function() {
  $expiration = 60*60*60*60*60*4;
  return $expiration;
});

// ACF GOOGLE MAP API KEY

function add_acf_google_maps_api_key() {
  acf_update_setting('google_api_key', 'AIzaSyC269jhw_Zukvh29_SicpI7pB7Ju8RZ1Ts');
}
add_action('acf/init', 'add_acf_google_maps_api_key');

// REMOVE DEFAULT USER ROLES

function update_user_roles() {
  remove_role( 'subscriber' );
  remove_role( 'editor' );
  remove_role( 'contributor' );
  remove_role( 'author' );
  
  add_role('senior', __('Senior'), array(
    'read' => true,
  ));

  add_role('user', __('User'), array(
    'read' => true,
    'create_posts' => true,
    'edit_posts' => true,
    'edit_others_posts' => true,
    'edit_published_posts' => true,
    'delete_posts' => true,
    'delete_published_posts' => true,
    'upload_files' => true,
    'publish_posts' => true,
    'events' => true,
    'edit_event' => true,
    'publish_event' => true     
  ));

}
add_action( 'init', 'update_user_roles' ); 

// SET DEFAULT IMAGE FOR ACF IMAGE FIELD

add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
function add_default_value_to_image_field($field) {
  $args = array(
    'label' => 'Default Image',
    'instructions' => 'Appears when creating a new post',
    'type' => 'image',
    'name' => 'default_value'
  );
  acf_render_field_setting($field, $args);
}

// Enqueue WP Media Scripts on ACF Field Group Edit Page

add_action('admin_enqueue_scripts', 'enqueue_uploader_for_image_default');
function enqueue_uploader_for_image_default() {
  $screen = get_current_screen();
  if ($screen && $screen->id && ($screen->id == 'acf-field-group')) {
    acf_enqueue_uploader();
  }
}

// MAKE User note, Duration, Start time, User & Senior Signature ACF FIELDS READ-ONLY

function make_acf_fields_read_only( $field ) {
  print '
  <script type="text/javascript">
    jQuery( "#acf-field_5de6ac4a5992c" ).prop( "disabled", true );
    jQuery( "#acf-field_5de6ad35bdd95" ).prop( "disabled", true );
    jQuery( "#acf-field_5e11c5b107e44" ).prop( "disabled", true );
    jQuery( "#acf-field_5e11c5f343097" ).prop( "disabled", true );
    jQuery( "#acf-field_5e11c8041a01c" ).prop( "disabled", true );
    jQuery( "#acf-field_5e1643aa97f41" ).prop( "disabled", true );
  </script>';
}  

add_action('acf/render_field/name=user_note', 'make_acf_fields_read_only');

// TEMPORARY ADJUST EXCERPT STYLES & MAKE EXCERPT READ_ONLY

function admin_excerpt()
{
  echo "<style>";
  echo "textarea#excerpt { height: 140px; }";
  echo "</style>";
  echo "<script type='text/javascript'>";
  echo "jQuery( 'textarea#excerpt' ).prop( 'disabled', true )";
  echo "</script>";
}
add_action('admin_footer', 'admin_excerpt');

// REGISTER COMPLETE EVENTS POST STATUS

function register_completed_status() {
	/** Statuses ******************************************************************/
	register_post_status( 'completed', array(
		'label'                     => _x( 'Completed', 'post status label', 'senjorogo' ),
		'public'                    => true,
		'label_count'               => _n_noop( 'Completed <span class="count">(%s)</span>', 'Completed <span class="count">(%s)</span>', 'senjorogo' ),
		'post_type'                 => array( 'events' ), // Define one or more post types the status can be applied to.
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'show_in_metabox_dropdown'  => true,
		'show_in_inline_dropdown'   => true,
		'dashicon'                  => 'dashicons-yes',
	) );
}
add_action( 'init', 'register_completed_status' );

// REGISTER "COMPLETED" ARGUMENT IN GraphQL
add_action( 'graphql_register_types', function() {
  register_graphql_field( 'RootQueryToEventConnectionWhereArgs', 'completedStatus', [
      'type' => 'boolean',
      'description' => __( 'Show events with comleted status', 'senjorogo' ),
  ] );
} );

// FILTER GraphQL query to respect "COMPLETED" status

// add_filter( 'graphql_post_object_connection_query_args', function( $query_args, $source, $args, $context, $info ) {
//   if ( isset( $args['where']['completedStatus'] ) && true === $args['where']['completedStatus'] ) {
//       //$sticky_ids = get_option( 'sticky_posts' );
//       //$event_status = get_field('event_status');
//       $query_args = [
//         //'numberposts'	=> -1,
//         //'post_type'		=> 'events',
//         'posts_per_page'	=> -1,
//         'orderby'			=> 'meta_value',
//         'order'				=> 'DESC',
//         'meta_key'		=> 'event_status',
//         'meta_value'	=> 'completed'
//       ];
//   }
//   return $query_args;
// }, 10, 5 );

/**
 * Only keep Publish, Draft & Pending statuses but do not use other builtin statuses.
 *
 * PS: you should at least keep Draft & Pending.
 *
 * @param  array  $post_types  The list of registered Post Types for the status.
 * @param  string $status_name Name of the status to apply to Post Types.
 * @return array               The list of registered Post Types for the status.
 */
function restrict_statuses_for_events( $post_types = array(), $status_name = '' ) {
	if ( 'draft' === $status_name || 'pending' === $status_name || 'publish' === $status_name ) {
		return $post_types;
	}
	// All other statuses (eg: Publish, Private...) won't be applied to events
	return array_diff( $post_types, array( 'events' ) );
}
add_filter( 'wp_statuses_get_registered_post_types', 'restrict_statuses_for_events', 10, 2 );

// HIDE UNWANTED FIELDS IN THE USER PROFILE

function hide_some_user_meta_fields() {
  $fieldsToHide = [
      'rich-editing',
      'admin-color',
      'comment-shortcuts',
      'admin-bar-front',
      'user-login',
      //'role',
      //'super-admin',
      //'first-name', 
      //'last-name', 
      //'nickname', 
      'display-name',
      //'email',
      'description', 
      //'pass1', 
      //'pass2', 
      //'sessions', 
      'capabilities',
      'syntax-highlighting',
      'url'
      ];
  
      //add the CSS
      foreach ($fieldsToHide as $fieldToHide) {
          echo '<style>tr.user-'.$fieldToHide.'-wrap{ display: none; }</style>';
      }
  
      //fields that don't follow the wrapper naming convention
      echo '<style>tr.user-profile-picture{ display: none; }</style>';
  
      //all subheadings
      echo '<style>#your-profile h2{ display: none; }</style>';
  }
  add_action( 'admin_head-user-edit.php', 'hide_some_user_meta_fields' );
  add_action( 'admin_head-profile.php',   'hide_some_user_meta_fields' );

// REMOVE POSTS, LINKS & COMMENTS FROM ADMIN DASHBOARD

function remove_menus () {
  global $menu;
  $restricted = array(__('Posts'), __('Links'),  __('Comments'));
  end ($menu);
  while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
  }
}
add_action('admin_menu', 'remove_menus');


// // FILTER EVENT CPT BY ACF DATE IN ADMIN AREA
// function gs_events_pre_get_posts($query) {

//   // Note: Use the $pagenow global for finer grained checking, 
//   // if you want to. There are many examples out there.

//   // Your question pertains to admin use only...

//   if (is_admin()) {

//       // Present the posts in your meta_key field's order in admin

//       if (isset($query->query_vars['post_type'])) {
//           if ($query->query_vars['post_type'] == 'events') {

//             $query->set('order', 'ASC');
//             $query->set('orderby', 'meta_value');
//             $query->set('meta_key', 'date_and_time');
//             $query->set('meta_type', 'DATETIME');

//               // ...and if you only want your future events to be 
//               // displayed in admin, you can uncomment the following:

//               // $query->set('meta_compare', '>=');
//               // $query->set('meta_value', time());

//               // Note: The use of time() is quick-and-dirty here, 
//               // in reality you'll need to adjust the timezone, 
//               // check server-time vs local time, etc.
//           }
//       }
//   }
// }

// add_filter('pre_get_posts' , 'gs_events_pre_get_posts');











// function my_update_value_date_time_picker( $value, $post_id, $field ) {
//     return strtotime( $value );
// }
// add_filter('acf/update_value/type=date_time_picker', 'my_update_value_date_time_picker', 10, 3);

// ASSIGN EVENT USER WITH ACF

function my_acf_save_post( $post_id ) {
  // get new value
  $user = get_field( 'user', $post_id );
  if( $user ) {
    wp_update_post( array( 'ID'=>$post_id, 'post_author'=>$user['ID']) ); 
  }
}
add_action('acf/save_post', 'my_acf_save_post', 20);

// AUTOGENERATE EVENT TITLE

function jb_update_postdata( $value, $post_id, $field ) {
  
  $senior = get_field('senior', $post_id);
  $date_and_time = get_field('date_and_time', $post_id);

  $date_start = strtotime(get_field('date_and_time', $post_id));
  $start = date_i18n('d M H:i', $date_start);
 
  $title = $senior['display_name'] . ' — ' . $start;
	$slug = sanitize_title( $title );
  
	$postdata = array(
	     'ID'	   => $post_id,
    	 'post_title'  => $title,
	     'post_type'   => 'events',
	     'post_name'   => $slug
  	);
  
	wp_update_post( $postdata );
	return $value;	
}

add_filter('acf/update_value/name=date_and_time', 'jb_update_postdata', 10, 3);
add_filter('acf/update_value/name=senior', 'jb_update_postdata', 10, 3);


// SHOW USER ROLES WITH ACF AS RADIO BUTTONS 

function showUserRoles( $field )
{	
	// reset choices
	$field['choices'] = array();
	
	global $wp_roles;
	$roles = $wp_roles->get_names();
	
	foreach ($roles as $role) {
		$field['choices'][ $role ] = $role;
	}

	return $field;
}

add_filter('acf/load_field/name=user_role', 'showUserRoles');


// SHOW ADDITIONAL INFORMATION FOR EVENTS IN ADMIN PANEL

function add_acf_columns ( $columns ) {
  return array_merge ( $columns, array ( 
    'date_and_time' => __ ( 'Event date' ),
    // 'event_status' => __ ( 'Status' ),
    'user' => __ ( 'User' ),
    'senior' => __ ( 'Senior' ),
  ) );
}
add_filter ( 'manage_events_posts_columns', 'add_acf_columns' );

function events_custom_column ( $column, $post_id ) {
  switch ( $column ) {
    case 'user':
      $user = get_field('user', $post_id);    
      echo $user['display_name'];
    break;
    
    case 'senior':
      $senior = get_field('senior', $post_id);    
      echo $senior['display_name'];  
    break;

    case 'date_and_time':
      $date_start = strtotime(get_post_meta($post_id, 'date_and_time', true));
      $start = date_i18n('d M H:i, Y', $date_start);    
      echo $start;
    break;
    
    // case 'event_status':
    //   echo get_post_meta ( $post_id, 'event_status', true );
    // break;
  }
}
add_action ( 'manage_events_posts_custom_column', 'events_custom_column', 10, 2 );

/* Make the column sortable */
function set_custom_events_sortable_columns( $columns ) {
  $columns['user'] = 'user';
  $columns['senior'] = 'senior';
  $columns['date_and_time'] = 'date_and_time';
  // $columns['event_status'] = 'event_status';

  return $columns;
}
add_filter( 'manage_edit-events_sortable_columns', 'set_custom_events_sortable_columns' );

function events_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get('orderby');

  if ( 'event_date' == $orderby ) {
    $query->set( 'meta_key', 'date_and_time' );
    $query->set( 'orderby', 'meta_value' );
  }
}
add_action( 'pre_get_posts', 'events_custom_orderby' );


// SAVE USER ROLES ACF AS WP_USER_ROLE

function acf_save_user( $post_id ) {
  // get new value
  $user_role = get_field( 'user_role', $post_id );
  if( $user_role ) {
    wp_update_user( array( 'ID'=>$post_id, 'role'=>$post_id) );
  }
}
add_action('acf/save_post', 'acf_save_user', 20);

// function acf_save_user_role( $post_id, $user_id ) {
//     // get new value
//     $user_role = get_field( 'user_role', $post_id );
//     update_field( $my_tst, $user_role, $post_id );
//     $user_id_role    = new WP_User($user_id);          
//     $user_id_role->set_role($user_role);
//     if( $user_role ) {
//         wp_update_user( array( 'ID'=>$post_id, 'post_author'=>$user['ID']) ); 
//     }
// }

add_action('acf/save_post', 'acf_save_user_role', 20);
//add_filter('acf/update_value/name=tst', 'my_acf_save_post2', 10, 3);
//add_filter('acf/load_field/name=tst', 'my_acf_save_post2');

add_action( 'user_profile_update_errors', 'crf_user_profile_update_errors', 10, 3 );
function crf_user_profile_update_errors( $errors, $update, $user ) {
	if ( $update ) {
		return;
	}

	if ( empty( $_POST['first_name'] ) ) {
		$errors->add( 'first_name_error', __( '<strong>ERROR</strong>: Please enter First Name.', 'sejorogo' ) );
	}

	if ( empty( $_POST['last_name'] ) ) {
		$errors->add( 'last_name_error', __( '<strong>ERROR</strong>: Please enter Last Name.', 'senjorogo' ) );
	}
}

// REGISTER SENJORO TYPE FOR GRAPHQL

add_action( 'graphql_register_types', 'register_senjoro_type' );

function register_senjoro_type() {
    register_graphql_object_type( 'Senjoro', [
      'description' => __( "Senjoro basic contact information", 'senjoro' ),
      'fields' => [
        'address' => [
          'type' => 'String',
          'description' => __( 'Senjoro address', 'senjorogo' ),
      ],
        'phone' => [
            'type' => 'String',
            'description' => __( 'Senjoro phone', 'senjoro' ),
        ],
        'email' => [
            'type' => 'String',
            'description' => __( 'Senjoro e-mail address', 'senjoro' ),
        ],
        'url' => [
            'type' => 'String',
            'description' => __( 'Senjoro URL address', 'senjoro' ),
        ],
      ],
    ] );
}

// GET SENJORO TYPE DATA FROM THEME OPTIONS

add_action( 'graphql_register_types', 'register_senjoro_field' );

function register_senjoro_field() {

    register_graphql_field( 'RootQuery', 'senjoro', [
      'description' => __( 'Get Senjoro contact information', 'senjoro' ),
      'type' => 'Senjoro',
      'resolve' => function() {

        return [
            'address' => of_get_option('address'),
            'phone' => of_get_option('phone'),
            'email' => of_get_option('email'),
            'url' => of_get_option('url')
        ];

      }
    ] );
}

